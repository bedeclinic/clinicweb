﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class Registrant : Worker
    {
        public Registrant() : base()
        {
        }
        public Registrant(string firstName, string lastName, DateTime dateofBirth, DateTime hireDate)
            :base(firstName,lastName,dateofBirth,hireDate)
        {
        }
        public virtual List<Visit> Visits { get; set; }
    }
}