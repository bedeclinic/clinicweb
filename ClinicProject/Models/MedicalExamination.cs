﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public enum MedicalExaminationState
    {
        CanceledByTechnician, CanceledByChief, Done, Pending, Approved
    }
    public enum ExaminationType
    {
        Physical, Laboratory
    }
    public class MedicalExamination
    {
        public MedicalExamination()
        {
            State = MedicalExaminationState.Pending;
            ExecutionDate = null;
            EntryDate = DateTime.Now;
            AcceptanceDate = null;
            ChiefTechnicianId = null;
            LaboratoryTechnicianId = null;

        }
        public int Id { get; set; }
        
        public DateTime EntryDate { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public DateTime? AcceptanceDate { get; set; }

        public string Description { get; set; }

        public MedicalExaminationState? State { get; set; }

        public string Result { get; set; }

        public string Comment { get; set; }

        public ExaminationType? Type { get; set; }
        [Required]
        public int? VisitId { get; set; }
        public virtual Visit Visit { get; set; }

        public int? LaboratoryTechnicianId { get; set; }
        public virtual LaboratoryTechnician LaboratoryTechnician { get; set; }

        public int? ChiefTechnicianId { get; set; }
        public virtual ChiefTechnician ChiefTechnician { get; set; }
        
        [ForeignKey("ExaminationDictionary")]
        public string Code { get; set; }
        public virtual ExaminationDictionary ExaminationDictionary { get; set; }
        
    }
}