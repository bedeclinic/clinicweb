﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class VisitViewModel
    {
        public IEnumerable<Visit> Visits { get; set; }
        public string Role { get; set; }
    }
}