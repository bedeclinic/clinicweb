﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class MyContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Patient> Patients { get; set; }

        public DbSet<Visit> Visits { get; set; }

        public DbSet<Worker> Workers { get; set; }

        public DbSet<MedicalExamination> MedicalExaminations { get; set; }

        public DbSet<ExaminationDictionary> ExaminationDictionary { get; set; }

        public MyContext():base("DefaultConnection")
        {            
        }
    }
}