﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class Doctor : Worker
    {
        public Doctor() : base()
        {
        }
        public Doctor(string firstName, string lastName, DateTime dateofBirth, DateTime hireDate, string license, string specialty)
            : base(firstName, lastName, dateofBirth, hireDate)
        {
            License = license;
            Specialty = specialty;
        }
        [Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "{0} can contain only digits!")]
        public string License { get; set; }

        [Required]
        [MaxLength(30), MinLength(3)]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "{0} can contain only letters!")]
        public string Specialty { get; set; }

        public virtual List<Visit> Visits { get; set; }
    }
}