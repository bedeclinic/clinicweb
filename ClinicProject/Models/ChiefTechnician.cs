﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class ChiefTechnician : Worker
    {
        public ChiefTechnician() : base()
        {
        }
        public ChiefTechnician(string firstName, string lastName, DateTime dateofBirth, DateTime hireDate)
            :base(firstName,lastName,dateofBirth,hireDate)
        {
        }
        public virtual List<MedicalExamination> MedicalExaminations { get; set; }
    }
}