﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class MedicalExaminationsViewModel
    {
        public IEnumerable<MedicalExamination> MedicalExaminations { get; set; }
        public string Role { get; set; }
    }
}