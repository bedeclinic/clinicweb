﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class Patient
    {
        public Patient()
        {
            DateOfBirth = new DateTime(1992, 05, 20);
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(30),MinLength(3)]
        [RegularExpression("^[a-zA-Z'-']+$", ErrorMessage = "{0} can contain only letters and '-'! ")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(30),MinLength(3)]
        [RegularExpression("^[a-zA-Z'-']+$",ErrorMessage="{0} can contain only letters and '-' !")]
        public string LastName { get; set; }
        [Required]
        [StringLength(30)]
        public string Address { get; set; }
        [Required]
        [MaxLength(11),MinLength(11)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "{0} can contain only digits!")]
        public string Pesel { get; set; }
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime DateOfBirth { get; set; }
        [MaxLength(9),MinLength(9)]
        [RegularExpression("^[0-9]+$",ErrorMessage = "{0} can contain only digits!")]
        public string PhoneNumber { get; set; }       
        public virtual List<Visit> Visits { get; set; }

    }
}