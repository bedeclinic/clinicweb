﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class PatientViewModel
    {
        public Patient Patient { get; set; }
        public IEnumerable<Visit> Visits { get; set; }
        public IEnumerable<MedicalExamination> MedicalExaminations { get; set; }
    }
}