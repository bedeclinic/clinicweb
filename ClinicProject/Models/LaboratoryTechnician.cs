﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public class LaboratoryTechnician : Worker
    {
        public LaboratoryTechnician() : base()
        {
        }
        public LaboratoryTechnician(string firstName, string lastName, DateTime dateofBirth, DateTime hireDate)
            : base(firstName, lastName, dateofBirth, hireDate)
        {
        }
        public virtual List<MedicalExamination> MedicalExaminations { get; set; }
    }
}