﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public enum State
    {
        [Display(Name = "Canceled")]
        Canceled,
        [Display(Name = "InProgress")]
        InProgress,
        [Display(Name = "Registred")]
        Registred,
        [Display(Name = "Ended")]
        Ended,
        All
    }
    public class Visit
    {
        public Visit()
        {
            State = ClinicProject.Models.State.Registred; // default state of the Visit 
            RegistryDate = DateTime.Now;
            ExecuteDate = null;
        }
        public int Id { get; set; }
        [Required]
        [Display(Name = "Registration Date")]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh\\:mm}", ApplyFormatInEditMode = true)]
        public DateTime? RegistryDate { get; set; }

        public State? State { get; set; }
        [Display(Name = "Execution Date")]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh\\:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ExecuteDate { get; set; }
        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(200)]
        public string Diagonose { get; set; }

        [Required(ErrorMessage = "You must select a patient!")]       
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        [Required]
        [ForeignKey("Registrant")]
        public int RegistrantId { get; set; }
        public virtual Registrant Registrant { get; set; }

        [Required]
        [ForeignKey("Doctor")]
        public int DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }
        public virtual List<MedicalExamination> MedicalExaminations { get; set; }
    }
}