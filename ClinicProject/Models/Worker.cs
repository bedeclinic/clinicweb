﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    public enum Status
    {
        Active, Inactive
    }
    public abstract class Worker
    {
        public Worker()
	    {
            this.Status = ClinicProject.Models.Status.Active;
	    }
        public Worker(string firstName, string lastName, DateTime dateofBirth, DateTime hireDate)
        {
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateofBirth;
            HireDate = hireDate;
            this.Status = ClinicProject.Models.Status.Active;
        }
        public int Id { get; set; }
        [Required]
        [Display(Name = "First name")]
        [StringLength(30,MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(30,MinimumLength=3)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Hire Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }
        [Required]
        public Status? Status { get; set; }
     
    }
}