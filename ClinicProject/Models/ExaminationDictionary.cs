﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClinicProject.Models
{
    [Table("ExaminationDictionary")] // don't want a plural form as it's a single dictionary :)
    public class ExaminationDictionary
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public decimal Price { get; set; }

        public virtual List<MedicalExamination> MedicalExamination { get; set; }

    }
}