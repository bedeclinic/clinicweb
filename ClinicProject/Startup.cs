﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClinicProject.Startup))]
namespace ClinicProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
