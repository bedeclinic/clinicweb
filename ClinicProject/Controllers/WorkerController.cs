﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ClinicProject.Controllers
{
    public class WorkerController : Controller
    {
        private MyContext db = new MyContext();

        // GET: /Worker/
        public async Task<ActionResult> Index()
        {
            var workers = await db.Workers.ToListAsync();
            var ret = from w in workers
                      orderby w.GetType().Name
                      select w;
            return View(ret);
        }

        // GET: /Worker/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Worker worker = await db.Workers.FindAsync(id);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // GET: /Worker/Create
        public ActionResult Create()
        {
            List<string> roleNames = new List<string>{"Registrant", "Doctor", "Technician", "ChiefTechnician" };
            //roleNames.Sort();
            ViewBag.Role = new SelectList(roleNames);
            return View();
        }

        // POST: /Worker/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection fc)
        {
            string role = fc["Role"];
            string firstName = fc["FirstName"];
            string lastName = fc["LastName"];
            string hireDate = fc["HireDate"];
            string dob = fc["DateOfBirth"];
            string password = fc["Password"];
            string userName = fc["UserName"];
            if (ModelState.IsValid)
            {
                // user creation
                var store = new UserStore<ApplicationUser>(db);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = userName };
                user.FirstName = firstName;
                user.LastName = lastName;
                var hasher = new PasswordHasher();
                var hashedPassword = hasher.HashPassword(password);
                user.PasswordHash = hashedPassword;

                manager.Create(user);
                manager.AddToRole(user.Id, role);
                if (role == "Doctor")
                {
                    string license = fc["License"];
                    string specialty = fc["Specialty"];
                    Doctor doctor = 
                        new Doctor(firstName,lastName,DateTime.Parse(dob),DateTime.Parse(hireDate),license,specialty);
                    db.Workers.Add(doctor);
                }
                else if (role == "Registrant")
                {
                    Registrant registrant = new Registrant(firstName, lastName, DateTime.Parse(dob), DateTime.Parse(hireDate));
                    db.Workers.Add(registrant);
                }
                else if (role == "Technician")
                {
                    LaboratoryTechnician lt = new LaboratoryTechnician(firstName, lastName, DateTime.Parse(dob), DateTime.Parse(hireDate));
                    db.Workers.Add(lt);
                }
                else
                {
                    ChiefTechnician ct = new ChiefTechnician(firstName, lastName, DateTime.Parse(dob), DateTime.Parse(hireDate));
                    db.Workers.Add(ct);
                }
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: /Worker/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Worker worker = await db.Workers.FindAsync(id);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Worker/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="Id,FirstName,LastName,DateOfBirth,HireDate,Status")] Worker worker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(worker).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(worker);
        }

        // GET: /Worker/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Worker worker = await db.Workers.FindAsync(id);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Worker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Worker worker = await db.Workers.FindAsync(id);
            db.Workers.Remove(worker);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult GetPartial()
        {
            return PartialView("_DoctorPartial",db.Workers);
        }
    }
}
