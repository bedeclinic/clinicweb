﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicProject.Models;

namespace ClinicProject.Controllers
{
    public class ExaminationDictionaryController : Controller
    {
        private MyContext db = new MyContext();

        // GET: /ExaminationDictionary/
        public async Task<ActionResult> Index()
        {
            var examinationdictionary = db.ExaminationDictionary.Include(e => e.MedicalExamination);
            return View(await examinationdictionary.ToListAsync());
        }

        // GET: /ExaminationDictionary/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExaminationDictionary examinationdictionary = await db.ExaminationDictionary.FindAsync(id);
            if (examinationdictionary == null)
            {
                return HttpNotFound();
            }
            return View(examinationdictionary);
        }

        // GET: /ExaminationDictionary/Create
        public ActionResult Create()
        {
            ViewBag.Code = new SelectList(db.MedicalExaminations, "Id", "Description");
            return View();
        }

        // POST: /ExaminationDictionary/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="Code,Name,Description,Price")] ExaminationDictionary examinationdictionary)
        {
            if (ModelState.IsValid)
            {
                db.ExaminationDictionary.Add(examinationdictionary);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Code = new SelectList(db.MedicalExaminations, "Id", "Description", examinationdictionary.Code);
            return View(examinationdictionary);
        }

        // GET: /ExaminationDictionary/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExaminationDictionary examinationdictionary = await db.ExaminationDictionary.FindAsync(id);
            if (examinationdictionary == null)
            {
                return HttpNotFound();
            }
            ViewBag.Code = new SelectList(db.MedicalExaminations, "Id", "Description", examinationdictionary.Code);
            return View(examinationdictionary);
        }

        // POST: /ExaminationDictionary/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="Code,Name,Description,Price")] ExaminationDictionary examinationdictionary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(examinationdictionary).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Code = new SelectList(db.MedicalExaminations, "Id", "Description", examinationdictionary.Code);
            return View(examinationdictionary);
        }

        // GET: /ExaminationDictionary/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExaminationDictionary examinationdictionary = await db.ExaminationDictionary.FindAsync(id);
            if (examinationdictionary == null)
            {
                return HttpNotFound();
            }
            return View(examinationdictionary);
        }

        // POST: /ExaminationDictionary/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ExaminationDictionary examinationdictionary = await db.ExaminationDictionary.FindAsync(id);
            db.ExaminationDictionary.Remove(examinationdictionary);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
