﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicProject.Models;
using PagedList;

namespace ClinicProject.Controllers
{
    public class PatientController : Controller
    {
        private MyContext db = new MyContext();

        // GET: /Patient/
        public ActionResult Index(string sortOrder, string currentPeselFilter, string pesel, string currentLastNameFilter, string lastName, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSort = sortOrder == "Date" ? "Date_desc" : "Date";
            if (pesel != null)
                page = 1;
            else
                pesel = currentPeselFilter;
            if (lastName != null)
                page = 1;
            else
                lastName = currentLastNameFilter;

            ViewBag.CurrentLastNameFilter = lastName;
            ViewBag.CurrentPeselFilter = pesel;

            var patients = from p in db.Patients
                           select p;

            switch(sortOrder)
            {
                case "name_desc":
                    patients = patients.OrderByDescending(p => p.LastName);
                    break;
                case "Date":
                    patients = patients.OrderBy(p => p.DateOfBirth);
                    break;
                case "Date_desc":
                    patients = patients.OrderByDescending(p => p.DateOfBirth);
                    break;
                default:
                    patients = patients.OrderBy(p => p.LastName);
                    break;
            }

            if (!String.IsNullOrEmpty(pesel))
            {
                patients = patients.Where(s => s.Pesel.Contains(pesel));
            }
            if (!String.IsNullOrEmpty(lastName))
            {
                patients = patients.Where(s => s.LastName.Contains(lastName));
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(patients.ToPagedList(pageNumber,pageSize));
        }

        // GET: /Patient/Details/5
        public async Task<ActionResult> Details(int? id, int? visitId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var viewModel = new PatientViewModel();
                viewModel.Patient = await db.Patients.Where(i => i.Id == id.Value).SingleOrDefaultAsync();
                if (viewModel.Patient == null)
                {
                    return HttpNotFound();
                }
                if (viewModel.Patient.Visits.Count != 0)
                {
                    viewModel.Visits = viewModel.Patient.Visits;
                    if (visitId != null)
                    {
                        ViewBag.VisitId = visitId.Value;
                        viewModel.MedicalExaminations = viewModel.Visits
                            .Where(i => i.Id == visitId.Value).SingleOrDefault().MedicalExaminations;
                    }
                }
                return View(viewModel);
            }
        }

        // GET: /Patient/Create
        public ActionResult Create()
        {
            var patient = new Patient();
            return View(patient);
        }

        // POST: /Patient/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="Id,FirstName,LastName,Address,Pesel,DateOfBirth,PhoneNumber")] Patient patient)
        {            
            if (ModelState.IsValid)
            {
                db.Patients.Add(patient);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(patient);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Select(int id)
        {
            return RedirectToAction("Create", "Visit", new { id = id });
        }     
    }
}
