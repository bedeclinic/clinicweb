﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicProject.Models;
using Microsoft.AspNet.Identity;


namespace ClinicProject.Controllers
{
    public class MedicalExaminationController : Controller
    {
        private MyContext db = new MyContext();

        // GET: /MedicalExamination/
        public async Task<ActionResult> Index(MedicalExaminationState? medicalState)
        {
            var medicalexaminations = db.MedicalExaminations.Include(m => m.ChiefTechnician).Include(m => m.ExaminationDictionary).Include(m => m.LaboratoryTechnician).Include(m => m.Visit);
            medicalexaminations = from me in medicalexaminations
                                  where me.Type == ExaminationType.Laboratory
                                  select me;
            if (medicalState != null)
            {
                medicalexaminations = from me in medicalexaminations
                                      where me.State == medicalState
                                      orderby me.EntryDate
                                      select me;                  
            }
            var viewModel = new MedicalExaminationsViewModel();
            viewModel.MedicalExaminations = await medicalexaminations.ToListAsync();
            viewModel.Role = GetUser().Roles.First().Role.Name;
            List<MedicalExaminationState> states = new List<MedicalExaminationState>();
            states = Enum.GetValues(typeof(MedicalExaminationState)).Cast<MedicalExaminationState>().ToList();
            ViewBag.MedicalStates = new SelectList(states);
            return View(viewModel);
        }

        // GET: /MedicalExamination/Create
        [Authorize(Roles="Doctor")]
        public ActionResult Create(int? id)
        {
            ViewBag.Code = new SelectList(db.ExaminationDictionary, "Code", "Name");
            var types = Enum.GetValues(typeof(ExaminationType)).Cast<ExaminationType>().ToList();
            ViewBag.Type = new SelectList(types);
            MedicalExamination me = new MedicalExamination();
            if(id != null)
                me.VisitId = id.Value;
            return View(me);
        }

        // POST: /MedicalExamination/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Doctor")]
        public async Task<ActionResult> Create([Bind(Include="Id,EntryDate,Description,State,Result,Comment,Type,LaboratoryTechnicianId,ChiefTechnicianId,Code")] MedicalExamination medicalexamination,int? VisitId)
        {
            // to do implement validation !!!!!!
            if (VisitId != null) 
                medicalexamination.VisitId = VisitId.Value;
            if (medicalexamination.Type == ExaminationType.Physical)
                medicalexamination.State = MedicalExaminationState.Done;
            db.MedicalExaminations.Add(medicalexamination);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", "Visit", new { id = medicalexamination.VisitId});
        }

        // GET: /MedicalExamination/Edit/5
        [Authorize(Roles = "Technician")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MedicalExamination medicalexamination = await db.MedicalExaminations.FindAsync(id);
            if (medicalexamination == null)
            {
                return HttpNotFound();
            }
            medicalexamination.ExecutionDate = DateTime.Now;
            return View(medicalexamination);
        }

        // POST: /MedicalExamination/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Technician")]
        public async Task<ActionResult> Edit([Bind(Include="Id,ExecutionDate,Result,Comment")] MedicalExamination medicalexamination)
        {
            var temp = (from me in db.MedicalExaminations
                        where me.Id == medicalexamination.Id
                        select me).Single();
            var user = GetUser();
            temp.ExecutionDate = DateTime.Now;
            temp.State = MedicalExaminationState.Done;
            temp.Result = medicalexamination.Result;
            temp.Comment = medicalexamination.Comment;
            temp.LaboratoryTechnicianId = (from w in db.Workers
                                          where w.FirstName == user.FirstName && w.LastName == user.LastName
                                          select w.Id).Single();
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private ApplicationUser GetUser()
        {
            var userId = User.Identity.GetUserId();
            var user = db.Users.Find(userId);
            if (user != null)
                return user;
            return null;
        }

        [Authorize(Roles ="ChiefTechnician,Technician")]
        public async Task<ActionResult> Cancel(int id, string role)
        {
            var examination = (from me in db.MedicalExaminations
                      where me.Id == id
                      select me).Single();
            if (role == "ChiefTechnicnian")
            {
                examination.State = MedicalExaminationState.CanceledByChief;
                examination.AcceptanceDate = DateTime.Now;
            }
            else
            {
                examination.State = MedicalExaminationState.CanceledByTechnician;
                examination.ExecutionDate = DateTime.Now;
            }
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "ChiefTechnician")]
        public async Task<ActionResult> Approve(int id, string role)
        {
            var examination = (from me in db.MedicalExaminations
                               where me.Id == id
                               select me).Single();
            examination.State = MedicalExaminationState.Approved;
            examination.AcceptanceDate = DateTime.Now;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
