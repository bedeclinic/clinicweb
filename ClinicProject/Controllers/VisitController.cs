﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ClinicProject.Models;
using System.Text;
using Microsoft.AspNet.Identity;


namespace ClinicProject.Controllers
{
    public class VisitController : Controller
    {
        private MyContext db = new MyContext();

        // GET: /Visit/
        public async Task<ActionResult> Index(State? state)
        {
            // doctor should only see his visits 
            //eager loading
            var user = GetUser();
            var visits = db.Visits
                .Include(v => v.Patient)
                .Include(v => v.Registrant)
                .Include(v => v.Doctor)
                .OrderBy(v => v.RegistryDate);
            var viewModel = new VisitViewModel();
            if (user.Roles.First().Role.Name == "Doctor")
            {
                visits = from v in visits
                         where v.Doctor.LastName == user.LastName && v.Doctor.FirstName == user.FirstName
                         orderby v.RegistryDate
                         select v;
            }
            if (state != null && state != State.All)
            {
                visits = from v in visits
                         where v.State == state.Value
                         orderby v.RegistryDate
                         select v;
            }
            visits = visits.OrderByDescending(v => v.RegistryDate);
            viewModel.Visits = await visits.ToListAsync();
            viewModel.Role = user.Roles.First().Role.Name;
            var states = new List<State>();
            states = Enum.GetValues(typeof(State)).Cast<State>().ToList();
            ViewBag.States = new SelectList(states);
            return View(viewModel);
        }

        // GET: /Visit/Create
        [Authorize(Roles = "Registrant")]
        public ActionResult Create(int? id)
        {
            if (id != null)
                ViewBag.PatientId = GetPatientInfo(id);
            ViewBag.DoctorId = new SelectList(db.Workers.OfType<Doctor>(), "Id", "FirstName");
            return View(new Visit());
        }

        // POST: /Visit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Registrant")]
        public async Task<ActionResult> Create([Bind(Include="Id,RegistryDate,State,ExecuteDate,Description,Diagonose,DoctorId")] Visit visit, int? id)
        {
            if(id != null)
                visit.PatientId = id.Value;
            if (ModelState.IsValid)
            {
                var user = GetUser();
                int registrantId = (from w in db.Workers
                                    where w.FirstName == user.FirstName && w.LastName == user.LastName
                                    select w.Id).Single();
                visit.RegistrantId = registrantId;
                db.Visits.Add(visit);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            if (id != null)
                ViewBag.PatientId = GetPatientInfo(id);
            ViewBag.DoctortId = new SelectList(db.Workers.OfType<Doctor>(), "Id", "FirstName", visit.DoctorId);
            return View(visit);
        }

        // GET: /Visit/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var visit = await db.Visits.FindAsync(id);
            if (visit == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegistrantId = new SelectList(db.Workers.OfType<Registrant>(), "Id", "FirstName", visit.RegistrantId);
            ViewBag.DoctortId = new SelectList(db.Workers.OfType<Doctor>(), "Id", "FirstName", visit.DoctorId);
            return View(visit);
        }

        // POST: /Visit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Diagonose")] Visit visit)
        {
            if (ModelState.IsValid)
            {
                var temp = db.Visits.SingleOrDefault(v => v.Id == visit.Id);
                temp.Description = visit.Description;
                temp.Diagonose = visit.Diagonose;
                temp.ExecuteDate = DateTime.Now;
                temp.State = State.Ended;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(visit);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private string GetPatientInfo(int? id)
        {
            var patient = (from p in db.Patients
                          where p.Id == id
                          select p).Single();
            StringBuilder temp = new StringBuilder();
            temp.Append(patient.FirstName);
            temp.Append(" ");
            temp.Append(patient.LastName);
            temp.Append(" ");
            temp.Append(patient.Pesel);
            return temp.ToString();
        }

        [Authorize(Roles="Doctor")]
        public async Task<ActionResult> Start(int? id)
        {
            var visit = from v in db.Visits
                        where v.Id == id.Value
                        select v;
            if(visit.Single().State == State.Registred)
                visit.Single().State = State.InProgress;
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = id.Value });
        }

        [Authorize(Roles = "Registrant")]
        public async Task<ActionResult> Cancel(int? id)
        {
            var visit = from v in db.Visits
                        where v.Id == id.Value
                        select v;
            if (visit.Single().State == State.Registred) // u can only cancel the registred ones!
                visit.Single().State = State.Canceled;
            visit.Single().ExecuteDate = DateTime.Now;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        private ApplicationUser GetUser()
        {
            var userId = User.Identity.GetUserId();
            var user = db.Users.Find(userId);
            if (user != null)
                return user;
            return null;
        }
        public ActionResult GetPatientByName(string firstName, string lastName)
        {
            var patient = (from p in db.Patients
                           where p.FirstName == firstName && p.LastName == lastName
                           select p).SingleOrDefault();
            return RedirectToAction("Details", "Patient", new { id = patient.Id });
        }

        public ActionResult AddExamination(int? id)
        {
            return RedirectToAction("Create", "MedicalExamination", new { id = id.Value });
        }

        public async Task<ActionResult> CancelVisit(int id)
        {
            var visit = await db.Visits.SingleOrDefaultAsync(v => v.Id == id);

            visit.State = State.Canceled;
            visit.Description = "Canceled by Doctor";
            visit.ExecuteDate = DateTime.Now;

            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public ActionResult UpdateDoctorList(string date)
        {
            var dateTime = Convert.ToDateTime(date);
            var model = new Tuple<Visit, DateTime>(new Visit(), dateTime);
            return PartialView("_DoctorList", model);
        }
    }
}
