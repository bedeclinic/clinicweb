namespace ClinicProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MedicalExaminations", "ChiefTechnicianId", c => c.Int(nullable: false));
            CreateIndex("dbo.MedicalExaminations", "ChiefTechnicianId");
            AddForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers");
            DropIndex("dbo.MedicalExaminations", new[] { "ChiefTechnicianId" });
            DropColumn("dbo.MedicalExaminations", "ChiefTechnicianId");
        }
    }
}
