namespace ClinicProject.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Migrations;
    using Microsoft.AspNet.Identity;
    using ClinicProject.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ClinicProject.Models.MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ClinicProject.Models.MyContext context)
        {
            //  This method will be called after migrating to the latest version.

            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            
            //create user roles
            string[] roleNames = { "Admin", "Registrant", "Doctor", "Technician", "ChiefTechnician" };
            IdentityResult roleResult;
            foreach (string item in roleNames)
            {
                if (!RoleManager.RoleExists(item))
                    roleResult = RoleManager.Create(new IdentityRole(item));
            }

            //filling examinationDictionary

            //var entries = new List<ExaminationDictionary>();
            //string filePath = @"C:\Users\Trzewior\Documents\Visual Studio 2013\Projects\ClinicProject\ClinicProject\bin\cos.txt";
            //if (System.IO.File.Exists(filePath))
            //{
            //    string[] lines = System.IO.File.ReadAllLines(filePath);
            //    foreach (string line in lines)
            //    {
            //        string trimmedLine = line.TrimEnd().TrimStart();
            //        string[] split = trimmedLine.Split('\t');
            //        entries.Add(new ExaminationDictionary { Code = split[0], Name = split[1], Price = Decimal.Parse(split[2]) });
            //    }
            //}
            //entries.ForEach(e => context.ExaminationDictionary.AddOrUpdate(p => p.Code, e));
        }
    }
}
