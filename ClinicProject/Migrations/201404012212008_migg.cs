namespace ClinicProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migg : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MedicalExaminations", "Type", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MedicalExaminations", "Type", c => c.Int(nullable: false));
        }
    }
}
