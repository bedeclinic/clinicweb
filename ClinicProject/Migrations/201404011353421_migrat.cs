namespace ClinicProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MedicalExaminations", "ExecutionDate", c => c.DateTime());
            AlterColumn("dbo.MedicalExaminations", "AcceptanceDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MedicalExaminations", "AcceptanceDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MedicalExaminations", "ExecutionDate", c => c.DateTime(nullable: false));
        }
    }
}
