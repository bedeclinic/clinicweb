namespace ClinicProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrato : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers");
            DropForeignKey("dbo.MedicalExaminations", "LaboratoryTechnicianId", "dbo.Workers");
            DropIndex("dbo.MedicalExaminations", new[] { "ChiefTechnicianId" });
            DropIndex("dbo.MedicalExaminations", new[] { "LaboratoryTechnicianId" });
            AlterColumn("dbo.MedicalExaminations", "LaboratoryTechnicianId", c => c.Int());
            AlterColumn("dbo.MedicalExaminations", "ChiefTechnicianId", c => c.Int());
            CreateIndex("dbo.MedicalExaminations", "ChiefTechnicianId");
            CreateIndex("dbo.MedicalExaminations", "LaboratoryTechnicianId");
            AddForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers", "Id");
            AddForeignKey("dbo.MedicalExaminations", "LaboratoryTechnicianId", "dbo.Workers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MedicalExaminations", "LaboratoryTechnicianId", "dbo.Workers");
            DropForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers");
            DropIndex("dbo.MedicalExaminations", new[] { "LaboratoryTechnicianId" });
            DropIndex("dbo.MedicalExaminations", new[] { "ChiefTechnicianId" });
            AlterColumn("dbo.MedicalExaminations", "ChiefTechnicianId", c => c.Int(nullable: false));
            AlterColumn("dbo.MedicalExaminations", "LaboratoryTechnicianId", c => c.Int(nullable: false));
            CreateIndex("dbo.MedicalExaminations", "LaboratoryTechnicianId");
            CreateIndex("dbo.MedicalExaminations", "ChiefTechnicianId");
            AddForeignKey("dbo.MedicalExaminations", "LaboratoryTechnicianId", "dbo.Workers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MedicalExaminations", "ChiefTechnicianId", "dbo.Workers", "Id", cascadeDelete: true);
        }
    }
}
