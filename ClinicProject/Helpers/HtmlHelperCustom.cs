﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ClinicProject.Models;

namespace ClinicProject.Helpers
{
    public static class HtmlHelperCustom
    {
        public static MvcHtmlString DropDownDoctorsFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttrubutes, string name = "", DateTime? date = null)
        {
            var htmlAttr = GenerateHtmlAttributes(htmlAttrubutes);

            var db = new MyContext();

            var doctors = db.Workers.OfType<Doctor>().ToList();
            date = date ?? DateTime.Now;
            var doctorsListItem = doctors.Select(doctor => new SelectListItem()
            {
                Text = doctor.FirstName + " " + doctor.LastName + " | " + WorkerHelper.GetNumberOfPatientsForDoctor(doctor.Id, date.Value) ,
                Value = doctor.Id.ToString()
            });


            if (name == "")
                name = ExpressionHelper.GetExpressionText(expression);
            var value = expression.Compile()(helper.ViewData.Model).ToString();

            StringBuilder dropdown = new StringBuilder();
            dropdown.Append(String.Format(@"<select name='{0}' {1}>", name, htmlAttr));
            string selected = "";
            foreach (var item in doctorsListItem)
            {
                if (value == item.Value)
                    selected = "selected='selected'";
                else selected = "";
                dropdown.Append(String.Format("<option value='{0}' {1} >{2}</option>", item.Value, selected, item.Text));
            }
            dropdown.Append(@"</select>");

            return MvcHtmlString.Create(dropdown.ToString());
        }
        private static string GenerateHtmlAttributes(object htmlAttrubutes)
        {
            Type myType = htmlAttrubutes.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            StringBuilder htmlAttr = new StringBuilder();
            foreach (PropertyInfo prop in props)
            {
                htmlAttr.Append(prop.Name.Replace('_', '-'));
                htmlAttr.Append("='");
                htmlAttr.Append(prop.GetValue(htmlAttrubutes, null).ToString());
                htmlAttr.Append("'");
            }
            return htmlAttr.ToString();
        }
    }
}