﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClinicProject.Models;

namespace ClinicProject.Helpers
{
    public static class WorkerHelper
    {
        public static int GetNumberOfPatientsForDoctor(int id, DateTime date)
        {
            var db = new MyContext();
            var today = date;
            var doctorVisits = db.Visits.Where(v => v.DoctorId == id).ToList();
            if (doctorVisits.Count == 0)
                return 0;

            var doctorVisitsForToday = doctorVisits
                .Where(v => v.RegistryDate.Value.Year == today.Year
                            && v.RegistryDate.Value.Month == today.Month
                            && v.RegistryDate.Value.Day == today.Day);
        
            return doctorVisitsForToday.Count();

        }
    }
}